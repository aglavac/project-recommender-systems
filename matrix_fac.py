"""
Factorization approach using a self-coded learning procedure.

X = W.dot(Z), where W and Z are learned using gradient descent with regularization.

Input to the model is the matrix X with unobserved entries, output is the full matrix X'.
"""

import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from utils import read_ratings, create_mappings


def init_matrix_fact(m, n, k, use_bias=False):
    """
    Randomly initialize the user and movie matrices W and Z between 0 and 1. If bias is used,
    add a column with all ones and another random column.
    :param m: number of users
    :param n: number of movies
    :param k: latent dimension
    :param use_bias: True if user and movie bias should be used.
    :return: Returns randomly between 0 and 1 initialized W and Z matrices.
    """
    W = np.abs(np.random.uniform(low=0, high=1, size=(m, k)))
    Z = np.abs(np.random.uniform(low=0, high=1, size=(n, k)))
    if use_bias:
        bias_W = np.stack((np.random.uniform(low=0, high=1, size=(m,)), np.ones((m,))), axis=1)
        W = np.concatenate((W, bias_W), axis=1)
        bias_Z = np.stack((np.ones((n,)), np.random.uniform(low=0, high=1, size=(n,))), axis=1)
        Z = np.concatenate((Z, bias_Z), axis=1)
    return W, Z


def gradient_descent(X, W, Z, alpha, lambda_, max_iter=1000, use_bias=False):
    """
    Given a matrix X with unobserved entries and two matrices W and Z with X = W.dot(Z), factorize X into W and Z.
    This is done using gradient descent.
    :param X: User, movie matrix with unobserved entries
    :param W: User matrix
    :param Z: Movie matrix
    :param alpha: learning rate
    :param lambda_: regularization factor
    :param max_iter: number of maximum iterations
    :param use_bias: True if user and movie biases should be learned.
    :return: return W and Z that minimize a loss X - W.dot(Z)
    """
    # mask X so that loss only calculated on unobserved entries
    X_masked = np.ma.masked_invalid(X)
    m = X.shape[0]  # number of users
    n = X.shape[1]  # number of movies
    for i in range(max_iter):
        WZ = W.dot(Z.transpose())
        WZ_masked = np.ma.masked_array(WZ, mask=X_masked.mask)  # we only care about observed entries
        E = np.ma.filled(X_masked - WZ_masked, fill_value=0)  # error between X and W.dot(Z)
        if i % 100 == 0:
            print('Loss: {0:.2f}'.format(loss(E, W, Z, lambda_)))
        # calc new W and Z using error matrix E
        W = W * (1 - alpha * lambda_) + alpha * E.dot(Z)
        Z = Z * (1 - alpha * lambda_) + alpha * E.transpose().dot(W)
        # if bias is used the one columns need to be reset to all ones
        if use_bias:
            W[:, -1] = np.ones((m,))
            Z[:, -2] = np.ones((n,))
    return W, Z


def loss(E, W, Z, lambda_):
    e_frob = np.linalg.norm(E, ord='fro') ** 2
    w_frob = np.linalg.norm(W, ord='fro') ** 2
    z_frob = np.linalg.norm(Z, ord='fro') ** 2
    return 0.5 * e_frob + 0.5 * lambda_ * w_frob + 0.5 * lambda_ * z_frob


def train():
    """
    Put all the previous functions together to build a training routine-
    """
    # load the movielens data
    df, _ = read_ratings("100k")
    # get user and movie numbers to build X
    num_users = len(df['userId'].unique())
    num_movies = len(df['movieId'].unique())
    print(f"Number of movies: {num_movies}\nNumber of users: {num_users}")
    # need mappings since userIds and movieIds not continuous
    mapping_X_to_df_movies, mapping_X_to_df_users = create_mappings(df)
    mapping_df_to_X_movies = {value: key for key, value in mapping_X_to_df_movies.items()}
    mapping_df_to_X_users = {value: key for key, value in mapping_X_to_df_users.items()}
    # remap Ids
    df['userId'] = df['userId'].apply(lambda x: mapping_df_to_X_users[x])
    df['movieId'] = df['movieId'].apply(lambda x: mapping_df_to_X_movies[x])

    df_train, df_test = train_test_split(df, stratify=df['userId'], test_size=0.1, random_state=12)
    df_train, df_validate = train_test_split(df_train, stratify=df_train['userId'], test_size=0.1, random_state=12)

    # convert dataframe to unobserved user/movie matrices for training, validation, and test
    X_train = np.full((num_users, num_movies), np.nan)
    for idx, row in df_train.iterrows():
        r = row['userId'].astype(int)
        c = row['movieId'].astype(int)
        X_train[r, c] = row['rating']

    X_test = np.full((num_users, num_movies), np.nan)
    for idx, row in df_test.iterrows():
        r = row['userId'].astype(int)
        c = row['movieId'].astype(int)
        X_test[r, c] = row['rating']

    X_validate = np.full((num_users, num_movies), np.nan)
    for idx, row in df_validate.iterrows():
        r = row['userId'].astype(int)
        c = row['movieId'].astype(int)
        X_validate[r, c] = row['rating']

    # hyperparams for training
    latent_features = 2
    num_iter = 3000
    m = X_train.shape[0]
    n = X_train.shape[1]
    # do training
    W, Z = init_matrix_fact(m, n, latent_features, use_bias=True)
    W, Z = gradient_descent(X_train, W, Z, 0.00005, 0.01, max_iter=num_iter, use_bias=True)
    # get result
    X_result = W.dot(Z.transpose())
    df_result = pd.DataFrame(X_result)
    # add bias
    df_result = df_result.stack().reset_index(name='price')
    df_result = df_result.rename(columns={'level_0': 'userId', 'level_1': 'movieId', 'price': 'rating'})
    df_accuracy = pd.merge(df_test, df_result, how='left', left_on=['userId', 'movieId'],
                           right_on=['userId', 'movieId'])
    rmse = ((df_accuracy.rating_x - df_accuracy.rating_y) ** 2).mean() ** .5
    print(f"RMSE: {rmse}")
    # save_pickle(W, "pickles/W.pickle")
    # save_pickle(Z, "pickles/Z.pickle")


if __name__ == '__main__':
    train()
