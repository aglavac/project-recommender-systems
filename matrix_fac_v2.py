"""
Factorization approach using Embedding layers provided by Keras.

User and movie embeddings are concatenated and then sent through a dot layer to calculate the dot product of the
embeddings.
Difference to matrix_fac.py: Instead of manually coding the training, using this approach we are able to use tensorflow
for training.

Input to the model are thus the movieId and userId, output is a rating for that combination.
"""

from utils import read_ratings, create_mappings

from sklearn.model_selection import train_test_split
import tensorflow.keras.layers as layers
from tensorflow.python.keras.models import Model
from tensorflow.python.keras.regularizers import l2
from tensorflow.python.keras.optimizer_v2.adam import Adam
from tensorflow.python.keras.metrics import mean_squared_error, RootMeanSquaredError


# load the data
df, _ = read_ratings("100k")
num_users = len(df['userId'].unique())
num_movies = len(df['movieId'].unique())
print(f"Number of movies: {num_movies}\nNumber of users: {num_users}")
mapping_X_to_df_movies, mapping_X_to_df_users = create_mappings(df)
mapping_df_to_X_movies = {value: key for key, value in mapping_X_to_df_movies.items()}
mapping_df_to_X_users = {value: key for key, value in mapping_X_to_df_users.items()}

df['userId'] = df['userId'].apply(lambda x: mapping_df_to_X_users[x])
df['movieId'] = df['movieId'].apply(lambda x: mapping_df_to_X_movies[x])

df_train, df_test = train_test_split(df, stratify=df['userId'], test_size=0.1, random_state=12)
df_train, df_validate = train_test_split(df_train, stratify=df_train['userId'], test_size=0.1, random_state=12)

# build model
latent_dim = 50  # dimension of the embeddings
user_input = layers.Input(shape=(1,), name='UserRating')
movie_input = layers.Input(shape=(1,), name='MovieRating')
user_embedding = layers.Embedding(num_users, latent_dim, embeddings_initializer='he_normal'
                                  , embeddings_regularizer=l2(1e-6))(user_input)
movie_embedding = layers.Embedding(num_movies, latent_dim, embeddings_initializer='he_normal'
                                   , embeddings_regularizer=l2(1e-6))(movie_input)
user_bias = layers.Embedding(num_users, 1)(user_input)
movie_bias = layers.Embedding(num_movies, 1)(movie_input)
user_movie = layers.Dot(axes=(2, 2))([user_embedding, movie_embedding])
output = layers.Add()([user_movie, user_bias, movie_bias])
model = Model(inputs=[user_input, movie_input], outputs=output)

model.compile(optimizer=Adam(learning_rate=0.001), loss=mean_squared_error, metrics=[RootMeanSquaredError(name='rmse')])
model.summary()

history = model.fit(
    x=[df_train['userId'], df_train['movieId']],  # two inputs, userId and movieId
    y=df_train['rating'],  # one output, the rating
    batch_size=64,
    epochs=7,
    verbose=1,
    validation_data=([df_validate['userId'], df_validate['movieId']], df_validate['rating']),
)



