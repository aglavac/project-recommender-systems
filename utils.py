import pickle
import pandas as pd
import numpy as np
import pathlib
import os

REPO_ROOT = pathlib.Path(__file__).absolute().parents[0].resolve()
assert (REPO_ROOT.exists())
CHECKPOINTS_PATH = (REPO_ROOT / "checkpoints").absolute().resolve()
assert (CHECKPOINTS_PATH.exists())
DATA_PATH = (REPO_ROOT / "data").absolute().resolve()
assert (DATA_PATH.exists())
MOVIELENS_1M = (DATA_PATH / "movielens_1m").absolute().resolve()
assert (MOVIELENS_1M.exists())
MOVIELENS_100K = (DATA_PATH / "movielens_100k").absolute().resolve()
assert (MOVIELENS_100K.exists())
MIND_SMALL = (DATA_PATH / "MINDsmall").absolute().resolve()
assert (MIND_SMALL.exists())
MIND_LARGE = (DATA_PATH / "MINDlarge").absolute().resolve()
assert (MIND_LARGE.exists())
PDFSPATH = (REPO_ROOT / "pdfs").absolute().resolve()
assert (PDFSPATH.exists())
PICKLESPATH = (REPO_ROOT / "pickles").absolute().resolve()
assert (PICKLESPATH.exists())
# value to fill unobserved entries
FILL_VALUE = 0


def read_ratings(size: str):
    """
    size: which movielens dataset to use
    """
    ml = "movielens_" + size
    if "m" in size:
        df = pd.read_csv("data/" + ml + "/ratings" + data_format(size), sep="::", header=None)
        df.columns = ["userId", "movieId", "rating", "timestamp"]
    elif "k" in size:
        df = pd.read_csv("data/" + ml + "/ratings" + data_format(size))
    df = df[['userId', 'movieId', 'rating']]
    df_pivoted = df.pivot(index='userId', columns='movieId', values='rating').sort_index()
    X = df_pivoted.to_numpy()
    return df, X


def train_test_split(df):
    df_work = df.copy()
    df_test = df_work.sample(frac=0.2)
    df_work = df_work.drop(df_test.index)
    X = df_work.pivot(index='userId', columns='movieId', values='rating').sort_index().to_numpy()
    return X, df_work, df_test


def create_mask(X, fill_value):
    """
    Numpy 2D-array with shape (movieId, userId)
    """
    return np.where(X == fill_value, 0, 1)


def mse():
    pass


def data_format(size):
    if "m" in size:
        return ".dat"
    elif "k" in size:
        return ".csv"


def read_movies():
    df = pd.read_csv("data/movielens_100k/movies.csv")
    df2 = df['genres'].str.get_dummies(sep='|')
    df = df.merge(df2, left_index=True, right_index=True)
    return df


def save_pickle(array, path):
    with open(path, 'wb') as f:
        pickle.dump(array, f)


def load_pickle(path):
    with open(path, 'rb') as f:
        array = pickle.load(f)
    return array


def create_mappings(df):
    """
    Creates Id mappings for users and movies, since Ids are not continuous.
    :param df: movielens dataframe to create mappings for
    :return: to dicts, one from Ids to indices in user/movie matrix, and one from user/movie matrix to Ids
    """
    df_pivoted = df.pivot(index='userId', columns='movieId', values='rating').sort_index()
    X = df_pivoted.to_numpy()
    mapping_movies = {}
    for i, c in enumerate(df_pivoted.columns):
        mapping_movies[i] = c
    mapping_users = {}
    for i, c in enumerate(df_pivoted.index):
        mapping_users[i] = c
    return mapping_movies, mapping_users


def plot_losses(ax, train_loss, valid_loss=None):
    # summarize _history for loss
    ax.plot(train_loss)
    if valid_loss:
        ax.plot(valid_loss)
    ax.set_title('model loss')
    ax.set_ylabel('loss')
    ax.set_xlabel('epoch')
    if valid_loss:
        ax.legend(['train', 'valid'], loc='upper left')
    else:
        ax.legend(['train'], loc='upper left')
