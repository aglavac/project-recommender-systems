from dataset import Dataset
from model_autoencoder import Autoencoder
from utils import save_pickle

import tensorflow as tf
import numpy as np


def kernel_gradients(autoenc: Autoencoder, dataset: Dataset):
    """
    Loss only calculated on unobserved values in input.

    Input: all ratings of users for one item where some entries are unobserved
    Output: reconstructed row with no unobserved entries
    """
    with tf.GradientTape() as gtape:
        x = np.array(dataset.X_train[0, :]).reshape((1, dataset.num_users))
        predictions = autoenc.model(x)
        grads = gtape.gradient(predictions, autoenc.model.trainable_weights)

    weights_to_draw = autoenc.model.get_layer('dense').get_weights()[0]
    minmin = np.min([np.min(x), np.min(grads[0]), np.min(weights_to_draw)])
    maxmax = np.max([np.max(x), np.max(grads[0]), np.max(weights_to_draw)])

    save_pickle({
        "weights to draw": weights_to_draw,
        "minmin": minmin,
        "maxmax": maxmax,
        "grads": grads,
        "x": x
    }, f"pickles/{str(dataset)}_kernel_weights_gradients.pickle")


def forgetting_replay(autoenc: Autoencoder, dataset: Dataset):
    """
    Loss only calculated on unobserved values in input.

    Here we train on the train data and afterward on number_new_days * 100 items (which are the test set) to quantify the
    amount of forgetting.
    During retraining we replay a certain fraction of the training data to combat catastrophic forgetting.
    """
    train_loss = autoenc.history()['masked_rmse_loss'].copy()
    valid_loss = autoenc.history()['val_masked_rmse_loss'].copy()
    test_loss = []

    class ForgettingCallback(tf.keras.callbacks.Callback):
        """
        Class to save the losses for all data sets while training
        """

        def on_epoch_end(self, epoch, logs=None):
            train_loss.append(
                self.model.evaluate(dataset.X_train, dataset.X_train, verbose=0)[1])  # [1] to get rmse
            valid_loss.append(
                self.model.evaluate(dataset.X_validate, dataset.X_validate, verbose=0)[1])  # [1] to get rmse
            test_loss.append(
                self.model.evaluate(dataset.X_test[i_prev: i, :], dataset.X_test[i_prev: i, :], verbose=0)[
                    1])  # [1] to get rmse

    i_prev = 0
    i = 100
    step_size = 100
    forgetting_callback = ForgettingCallback()
    no_steps = len(dataset.X_test) // 100
    for i in range(step_size, len(dataset.X_test), 100):
        print(f"Step: {i}/{no_steps}")
        replays = dataset.df_train.sample(frac=0.1).to_numpy()
        X_retrain = np.vstack((replays, dataset.X_test[i_prev: i, :]))
        # refit on never seen movies
        history = autoenc.model.fit(
            x=X_retrain,
            y=X_retrain,
            batch_size=1,
            epochs=5,
            verbose=2,
            validation_data=(dataset.X_validate, dataset.X_validate),
            callbacks=[forgetting_callback]
        )
        i_prev = i

    # plot the losses
    # fig, ax = plt.subplots(2, 1, figsize=(10, 5))
    # plot_losses(ax[0], train_loss[10:], valid_loss[10:])
    # ax[0].axvline(x=train_epochs, linestyle='--', c='r')
    # ax[0].set_xlim(left=0)
    # ax[1].plot(range(train_epochs, len(test_loss) + train_epochs), test_loss)
    # ax[1].set_ylabel('loss')
    # ax[1].set_xlabel('epoch')
    # ax[1].set_xlim(left=0)
    # ax[1].legend(['test'], loc='upper left')
    # ax[1].axvline(x=train_epochs, linestyle='--', c='r')
    # fig.tight_layout()
    # plt.savefig('forgetting_replay.pdf', bbox_inches='tight')
    # plt.show()


def forgetting_learning_rate(autoenc: Autoencoder, dataset: Dataset):
    """
    Loss only calculated on unobserved values in input.

    Here we train on the train data and afterward on new items (which are the test set) to quantify the amount of
    forgetting.
    More specifically, we are interested in what happens if we reduce the learning rate when retraining on the new items.
    """
    train_loss = autoenc.history()['masked_rmse_loss'].copy()
    valid_loss = autoenc.history()['val_masked_rmse_loss'].copy()
    test_loss = []

    class ForgettingCallback(tf.keras.callbacks.Callback):
        def on_epoch_end(self, epoch, logs=None):
            train_loss.append(
                self.model.evaluate(dataset.X_train, dataset.X_train, verbose=0)[1])  # [1] to get rmse
            valid_loss.append(
                self.model.evaluate(dataset.X_validate, dataset.X_validate, verbose=0)[1])  # [1] to get rmse
            test_loss.append(
                self.model.evaluate(dataset.X_test[i_prev: i, :], dataset.X_test[i_prev: i, :], verbose=0)[
                    1])  # [1] to get rmse

    i_prev = 0
    i = 100
    step_size = 100
    forgetting_callback = ForgettingCallback()
    autoenc.model.optimizer.lr.assign(0.0001)  # reduce learning rate to 1/10th of 1e-3
    no_steps = len(dataset.X_test) // 100
    for i in range(step_size, len(dataset.X_test), 100):
        print(f"Step: {i}/{no_steps}")
        # refit on never seen movies
        history = autoenc.model.fit(
            x=dataset.X_test[i_prev: i, :],
            y=dataset.X_test[i_prev: i, :],
            batch_size=1,
            epochs=5,
            verbose=2,
            validation_data=(dataset.X_validate, dataset.X_validate),
            callbacks=[forgetting_callback]
        )
        i_prev = i

    # plot the losses
    # fig, ax = plt.subplots(2, 1, figsize=(10, 5))
    # plot_losses(ax[0], train_loss[10:], valid_loss[10:])
    # ax[0].axvline(x=train_epochs, linestyle='--', c='r')
    # ax[0].set_xlim(left=0)
    # ax[1].plot(range(train_epochs, len(test_loss) + train_epochs), test_loss)
    # ax[1].set_ylabel('loss')
    # ax[1].set_xlabel('epoch')
    # ax[1].set_xlim(left=0)
    # ax[1].legend(['test'], loc='upper left')
    # ax[1].axvline(x=train_epochs, linestyle='--', c='r')
    # fig.tight_layout()
    # plt.savefig('forgetting_learning_rate.pdf', bbox_inches='tight')
    # plt.show()


def forgetting_no_retrain(autoenc: Autoencoder, dataset: Dataset):
    """
    Loss only calculated on unobserved values in input.

    Here we train on the train data and afterward on number_of_days * 100 items (which make up the test set) to
    quantify the amount of forgetting.
    More specifically, we are interested in the accuracy on the test set if we do not retrain on it.
    """
    # lists holding the accuracies during retraining for all data sets
    valid_loss = []
    train_loss = []
    test_loss = []

    i_prev = 0
    i = 100
    step_size = 100
    no_steps = len(dataset.X_test) // 100
    for i in range(step_size, len(dataset.X_test), 100):
        print(f"Step: {i}/{no_steps}")
        # calc loss on never seen movies
        train_loss.append(autoenc.model.evaluate(dataset.X_train, dataset.X_train, verbose=0)[1])  # [1] to get rmse
        valid_loss.append(
            autoenc.model.evaluate(dataset.X_validate, dataset.X_validate, verbose=0)[1])  # [1] to get rmse
        test_loss.append(
            autoenc.model.evaluate(dataset.X_test[i_prev: i, :], dataset.X_test[i_prev: i, :], verbose=0)[
                1])  # [1] to get rmse

    save_pickle({
        "mean_train_loss": train_loss,
        "mean_valid_loss": valid_loss,
        "mean_test_loss": test_loss,
    }, f"pickles/{str(dataset)}_forgetting_noretrain.pickle")


def epoch_tradeoff(latent_dim, kernel_reg, learning_rate, model_path, history_path, dataset: Dataset):
    """
    Loss only calculated on unobserved values in input.

    Here we train on the train data and afterward on 100 items (which are the test set) to quantify the amount of
    forgetting.
    More specifically, we are interested in the tradeoff between how much we train on the 100 new items vs. the
    accuracy on the training set, validation set and 100 new items.
    """
    # define all the epochs want to get accuracies for
    if 'movielens' in str(dataset):
        epochs = [1, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
        retrain_batch_size = 50
    else:
        epochs = [1, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]
        retrain_batch_size = 50
        # lists holding the losses for each epoch value
    trade_valid = []
    trade_train = []
    trade_test = []
    prev_epoch = 0
    autoenc = Autoencoder(dataset.num_users, latent_dim, kernel_reg, learning_rate, model_path=model_path,
                          history_path=history_path)
    idx = np.random.randint(low=0, high=dataset.X_train.shape[0], size=dataset.X_train.shape[0] // 10)
    X_train_eval = dataset.X_train[idx, :]
    for epoch_ in epochs:
        print(f"Epoch: {epoch_}")
        train_loss = []
        valid_loss = []
        test_loss = []

        class ForgettingCallback(tf.keras.callbacks.Callback):
            """
            Class to save the losses for all data sets while training
            """

            def on_epoch_end(self, epoch, logs=None):
                train_loss.append(
                    self.model.evaluate(X_train_eval, X_train_eval, verbose=0)[1])  # [1] to get rmse
                valid_loss.append(
                    self.model.evaluate(dataset.X_validate, dataset.X_validate, verbose=0)[1])  # [1] to get rmse
                test_loss.append(
                    self.model.evaluate(dataset.X_test[i_prev: i, :], dataset.X_test[i_prev: i, :], verbose=0)[
                        1])  # [1] to get rmse

        # # reload the model trained on only training data
        # autoenc = Autoencoder(dataset.num_users, latent_dim, kernel_reg, learning_rate, model_path=model_path,
        #                       history_path=history_path)
        i_prev = 0
        i = 100
        step_size = 100
        forgetting_callback = ForgettingCallback()
        for i in range(step_size, len(dataset.X_test) + 1, 100):
            # refit on never seen movies
            history = autoenc.model.fit(
                x=dataset.X_test[i_prev: i, :],
                y=dataset.X_test[i_prev: i, :],
                batch_size=retrain_batch_size,
                epochs=epoch_ - prev_epoch,
                verbose=2,
                validation_data=(dataset.X_validate, dataset.X_validate),
                callbacks=[forgetting_callback]  # pass the callback to log the accuracies
            )
            i_prev = i

        prev_epoch = epoch_
        if epoch_ < 10:
            trade_valid.append(np.mean(valid_loss))
            trade_train.append(np.mean(train_loss))
            trade_test.append(np.mean(test_loss))
        else:
            trade_valid.append(np.mean(valid_loss[-10:]))
            trade_train.append(np.mean(train_loss[-10:]))
            trade_test.append(np.mean(test_loss[-10:]))

    # save the results
    save_pickle({
        "epochs": epochs,
        "mean_train_rmse": trade_train,
        "mean_valid_rmse": trade_valid,
        "mean_test_rmse": trade_test,
    }, f"pickles/{str(dataset)}_forgetting_epoch_tradeoff.pickle")


def learning_rate_tradeoff(latent_dim, kernel_reg, learning_rate_old, model_path, history_path, dataset: Dataset):
    """
    Loss only calculated on unobserved values in input.

    Here we train on the train data and afterward on 100 items (which are the test set) to quantify the amount of
    forgetting.
    More specifically, we are interested in the tradeoff between the learning rate for the 100 new items vs. the
    accuracy on the training set, validation set and 100 new items.
    """
    # all the learning rates we want to analyze
    learning_rates = [1e-6, 5e-6, 1e-5, 5e-5, 1e-4, 5e-4, 1e-3, 5e-3, 1e-2]
    # lists holding losses during retraining
    trade_valid = []
    trade_train = []
    trade_test = []
    if 'movielens' in str(dataset):
        retrain_batch_size = 50
    else:
        retrain_batch_size = 50
    for learning_rate in learning_rates:
        print(f"Learning Rate: {learning_rate}")
        train_loss = []
        valid_loss = []
        test_loss = []

        class ForgettingCallback(tf.keras.callbacks.Callback):
            def on_epoch_end(self, epoch, logs=None):
                train_loss.append(
                    self.model.evaluate(dataset.X_train, dataset.X_train, verbose=0)[1])  # [1] to get rmse
                valid_loss.append(
                    self.model.evaluate(dataset.X_validate, dataset.X_validate, verbose=0)[1])  # [1] to get rmse
                test_loss.append(
                    self.model.evaluate(dataset.X_test[i_prev: i, :], dataset.X_test[i_prev: i, :], verbose=0)[
                        1])  # [1] to get rmse

        # reload the model trained on only training data
        autoenc = Autoencoder(dataset.num_users, latent_dim, kernel_reg, learning_rate_old, model_path=model_path,
                              history_path=history_path)

        # assign new learning rate
        autoenc.model.optimizer.lr.assign(learning_rate)
        i_prev = 0
        i = 100
        step_size = 100
        forgetting_callback = ForgettingCallback()
        for i in range(step_size, len(dataset.X_test) + 1, 100):
            # refit on never seen movies
            history = autoenc.model.fit(
                x=dataset.X_test[i_prev: i, :],
                y=dataset.X_test[i_prev: i, :],
                batch_size=retrain_batch_size,
                epochs=5,
                verbose=2,
                validation_data=(dataset.X_validate, dataset.X_validate),
                callbacks=[forgetting_callback]
            )
            i_prev = i

            trade_valid.append(np.mean(valid_loss))
            trade_train.append(np.mean(train_loss))
            trade_test.append(np.mean(test_loss))

    save_pickle({
        "learning_rates": learning_rates,
        "mean_train_loss": trade_train,
        "mean_valid_loss": trade_valid,
        "mean_test_loss": trade_test,
    }, f"pickles/{str(dataset)}_forgetting_learningrate_tradeoff.pickle")


def replay_tradeoff(latent_dim, kernel_reg, learning_rate_old, model_path, history_path, dataset: Dataset):
    """
    Loss only calculated on unobserved values in input.

    Here we train on the train data and afterward on 100 items (which are the test set) to quantify the amount of
    forgetting.
    Here we replay data of the training set to combat catastrophic forgetting.
    More specifically, we are interested in the tradeoff between which fraction of the training set we replay vs. the
    accuracy on the training set, validation set and 100 new items.
    """
    # the replay fraction for the training set we analyze
    replays = [0, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
    # lists holding the losses for all data sets
    trade_valid = []
    trade_train = []
    trade_test = []
    if 'movielens' in str(dataset):
        retrain_batch_size = 50
    else:
        retrain_batch_size = 50
    for replay in replays:
        print(f"Replay: {replay}")
        train_loss = []
        valid_loss = []
        test_loss = []

        class ForgettingCallback(tf.keras.callbacks.Callback):
            """
            Class to save the losses for all data sets while training
            """

            def on_epoch_end(self, epoch, logs=None):
                train_loss.append(
                    self.model.evaluate(dataset.X_train, dataset.X_train, verbose=0)[1])  # [1] to get rmse
                valid_loss.append(
                    self.model.evaluate(dataset.X_validate, dataset.X_validate, verbose=0)[1])  # [1] to get rmse
                test_loss.append(
                    self.model.evaluate(dataset.X_test[i_prev: i, :], dataset.X_test[i_prev: i, :], verbose=0)[
                        1])  # [1] to get rmse

        # reload the model trained on only training data
        autoenc = Autoencoder(dataset.num_users, latent_dim, kernel_reg, learning_rate_old,
                              model_path=model_path,
                              history_path=history_path)

        i_prev = 0
        i = 100
        step_size = 100
        forgetting_callback = ForgettingCallback()
        for i in range(step_size, len(dataset.X_test) + 1, 100):
            replayed = dataset.df_train.sample(frac=replay).to_numpy()
            X_retrain = np.vstack((replayed, dataset.X_test[i_prev: i, :]))
            # refit on never seen movies
            history = autoenc.model.fit(
                x=X_retrain,
                y=X_retrain,
                batch_size=retrain_batch_size,
                epochs=5,
                verbose=2,
                validation_data=(dataset.X_validate, dataset.X_validate),
                callbacks=[forgetting_callback]
            )
            i_prev = i

            trade_valid.append(np.mean(valid_loss))
            trade_train.append(np.mean(train_loss))
            trade_test.append(np.mean(test_loss))

    save_pickle({
        "replays": replays,
        "mean_train_loss": trade_train,
        "mean_valid_loss": trade_valid,
        "mean_test_loss": trade_test,
    }, f"pickles/{str(dataset)}_forgetting_replay_tradeoff.pickle")


if __name__ == '__main__':
    pass
