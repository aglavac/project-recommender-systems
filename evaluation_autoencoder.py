from model_autoencoder import create_model_mind, create_model_movie
from evaluation_functions import epoch_tradeoff, replay_tradeoff, learning_rate_tradeoff, forgetting_learning_rate, \
    forgetting_replay, forgetting_no_retrain, kernel_gradients

import argparse


def get_input_params(dataset, eval):
    params = dict()
    if dataset == 'movielens':
        params["dataset"] = "1m"
        params["batch_size"] = 64
        params["train_epochs"] = 3500
        params["latent_dim"] = 100  # dimension of the latent layer of Autoencoder
        params["kernel_reg"] = 1e-3  # regularizer for kernel of dense hidden layer
        params["learning_rate"] = 0.001
        if eval == "gradient" \
                or eval == "":
            params["test_split"] = 0.1
        elif eval == "epoch_tradeoff" \
                or eval == "learning_rate_tradeoff" \
                or eval == "replay_tradeoff":
            params["test_split"] = 100

        elif eval == "forgetting_learning_rate" \
                or eval == "forgetting_noretrain" \
                or eval == "forgetting_replay":
            params["test_split"] = 0.5
    elif dataset == 'MIND':
        params["dataset"] = "MINDsmall_train"
        params["batch_size"] = 256
        params["train_epochs"] = 500
        params["latent_dim"] = 100
        params["kernel_reg"] = 1e-3
        params["learning_rate"] = 0.001
        if eval == "epoch_tradeoff" \
                or eval == "learning_rate_tradeoff" \
                or eval == "replay_tradeoff":
            params["test_split"] = 100

        elif eval == "forgetting_learning_rate" \
                or eval == "forgetting_noretrain" \
                or eval == "forgetting_replay" \
                or eval == "":
            params["test_split"] = 0.1
    return params


def get_eval_fun(eval):
    if eval == "gradient":
        return kernel_gradients
    elif eval == "epoch_tradeoff":
        return epoch_tradeoff
    elif eval == "learning_rate_tradeoff":
        return learning_rate_tradeoff
    elif eval == "replay_tradeoff":
        return replay_tradeoff
    elif eval == "forgetting_learning_rate":
        return forgetting_learning_rate
    elif eval == "forgetting_noretrain":
        return forgetting_no_retrain
    elif eval == "forgetting_replay":
        return forgetting_replay


def get_model_fun(dataset):
    if dataset == 'movielens':
        return create_model_movie
    elif dataset == 'MIND':
        return create_model_mind


def do_eval(dataset, eval):
    params = get_input_params(dataset, eval)
    create_model = get_model_fun(dataset)
    dataset, autoenc, model_path, history_path = create_model(params["dataset"],
                                                              params["test_split"],
                                                              params["batch_size"],
                                                              params["train_epochs"],
                                                              params["latent_dim"],
                                                              params["kernel_reg"],
                                                              params["learning_rate"])
    eval_function = get_eval_fun(eval)
    if eval == "":
        pass
    elif eval == "epoch_tradeoff" \
            or eval == "learning_rate_tradeoff" \
            or eval == "replay_tradeoff":
        eval_function(params["latent_dim"], params["kernel_reg"], params["learning_rate"], model_path, history_path,
                      dataset)
    else:
        eval_function(autoenc, dataset)


def parse_args():
    parser = argparse.ArgumentParser(description='Evaluation script. All')
    parser.add_argument("--dataset",
                        type=str,
                        choices=['MIND', 'movielens', ],
                        required=True,
                        help="Which dataset to use.")
    parser.add_argument("--eval",
                        type=str,
                        choices=["gradient", "epoch_tradeoff", "learning_rate_tradeoff",
                                 "replay_tradeoff", "forgetting_learning_rate", "forgetting_noretrain",
                                 "forgetting_replay", "none"],
                        required=True,
                        help="Which evaluation to run.")
    return parser.parse_args()


def main(args):
    do_eval(args.dataset, args.eval)


if __name__ == '__main__':
    args = parse_args()
    # args = argparse.Namespace(dataset="MIND", eval="none")
    main(args)
