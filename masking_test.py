"""
File was created to test the masking class of Keras. In the end, it is not what we need to not train on unobserved
entries, since unobserved entries instead of being ignored, are filled with dummy values and still updated during
training.
"""
import numpy as np
import tensorflow as tf
import tensorflow.python.keras.layers as layers
from tensorflow.python.keras.models import Model

in_ = tf.Variable(tf.ones((1, 7)))
in_ = in_[0, 0].assign(-1)
# model = Sequential()
# model.add(layers.Input(shape=in_.shape))
# model.add(layers.Masking(mask_value=-2))
# model.add(layers.Dense(1))
# # model.add(layers.Dense(1, input_shape=in_.shape))
# # model.add(layers.Dense(1, name='hidden'))
# model.build()

input = layers.Input(shape=7)
masked_input = layers.Masking(mask_value=-1)(input)
output = layers.Dense(1)(masked_input)
model = Model(inputs=input, outputs=output)

weights = []
x = np.array([[1], [1], [1], [1], [1], [1], [1]])  # weights
y = np.array([0])  # array of biases
weights.append(x)
weights.append(y)
model.get_layer('dense').set_weights(weights)

x = in_
y = model(x)
print("~~~~~~~~~~~")
print(y)
print("~~~~~~~~~~~")
print(model.get_weights())
print("~~~~~~~~~~~")
print(model.summary())
print("~~~~~~~~~~~")
print(output._keras_mask)
