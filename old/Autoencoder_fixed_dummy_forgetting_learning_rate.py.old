"""
Autoencoder encoding the movies. Loss only calculated on unobserved values in input.

Here we train on the train data and afterward on 100 movies (which are the test set) to quantify the amount of
forgetting.
More specifically, we are interested in what happens if we reduce the learning rate when retraining on the new movies.
"""

from model_autoencoder import create_model_movie
from utils import plot_losses

import matplotlib.pyplot as plt
import tensorflow as tf

# load the data set
dataset = "1m"
test_split = 0.5
# define params for model
batch_size = 64
train_epochs = 3500
latent_dim = 100
kernel_reg = 1e-3
learning_rate = 0.001
movielens, autoenc, model_path, history_path = create_model_movie(dataset, test_split, batch_size, train_epochs, latent_dim,
                                                                  kernel_reg, learning_rate)

train_loss = autoenc.history()['masked_rmse_loss'].copy()
valid_loss = autoenc.history()['val_masked_rmse_loss'].copy()
test_loss = []


class ForgettingCallback(tf.keras.callbacks.Callback):
    def on_epoch_end(self, epoch, logs=None):
        train_loss.append(self.model.evaluate(movielens.X_train, movielens.X_train, verbose=0)[1])  # [1] to get rmse
        valid_loss.append(
            self.model.evaluate(movielens.X_validate, movielens.X_validate, verbose=0)[1])  # [1] to get rmse
        test_loss.append(
            self.model.evaluate(movielens.X_test[i_prev: i, :], movielens.X_test[i_prev: i, :], verbose=0)[
                1])  # [1] to get rmse


i_prev = 0
i = 100
step_size = 100
forgetting_callback = ForgettingCallback()
autoenc.model.optimizer.lr.assign(0.0001)  # reduce learning rate to 1/10th of 1e-3
for i in range(step_size, len(movielens.X_test), 100):
    # refit on never seen movies
    history = autoenc.model.fit(
        x=movielens.X_test[i_prev: i, :],
        y=movielens.X_test[i_prev: i, :],
        batch_size=1,
        epochs=5,
        verbose=2,
        validation_data=(movielens.X_validate, movielens.X_validate),
        callbacks=[forgetting_callback]
    )
    i_prev = i

# plot the losses
fig, ax = plt.subplots(2, 1, figsize=(10, 5))
plot_losses(ax[0], train_loss[10:], valid_loss[10:])
ax[0].axvline(x=train_epochs, linestyle='--', c='r')
ax[0].set_xlim(left=0)
ax[1].plot(range(train_epochs, len(test_loss) + train_epochs), test_loss)
ax[1].set_ylabel('loss')
ax[1].set_xlabel('epoch')
ax[1].set_xlim(left=0)
ax[1].legend(['test'], loc='upper left')
ax[1].axvline(x=train_epochs, linestyle='--', c='r')
fig.tight_layout()
plt.savefig('forgetting_learning_rate.pdf', bbox_inches='tight')
plt.show()

print("YOLO")
