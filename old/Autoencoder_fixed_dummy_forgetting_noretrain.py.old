"""
Autoencoder encoding the movies. Loss only calculated on unobserved values in input.

Here we train on the train data and afterward on number_of_days * 100 movies (which make up the test set) to
quantify the amount of forgetting.
More specifically, we are interested in the accuracy on the test set if we do not retrain on it.
"""

from utils import save_pickle
from model_autoencoder import create_model_movie

# load the data set
dataset = "1m"
test_split = 0.5
# define params for model
batch_size = 64
train_epochs = 2000
latent_dim = 100
kernel_reg = 1e-3
learning_rate = 0.001
movielens, autoenc, model_path, history_path = create_model_movie(dataset, test_split, batch_size, train_epochs, latent_dim,
                                                                  kernel_reg, learning_rate)

# lists holding the accuracies during retraining for all data sets
valid_loss = []
train_loss = []
test_loss = []

i_prev = 0
i = 100
step_size = 100
for i in range(step_size, len(movielens.X_test), 100):
    # calc loss on never seen movies
    train_loss.append(autoenc.model.evaluate(movielens.X_train, movielens.X_train, verbose=0)[1])  # [1] to get rmse
    valid_loss.append(
        autoenc.model.evaluate(movielens.X_validate, movielens.X_validate, verbose=0)[1])  # [1] to get rmse
    test_loss.append(autoenc.model.evaluate(movielens.X_test[i_prev: i, :], movielens.X_test[i_prev: i, :], verbose=0)[
                         1])  # [1] to get rmse

save_pickle({
    "mean_train_loss": train_loss,
    "mean_valid_loss": valid_loss,
    "mean_test_loss": test_loss,
}, "pickles/forgetting_noretrain.pickle")
