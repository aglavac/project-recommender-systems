import tensorflow.python.keras.layers as layers
from tensorflow.python.keras.optimizer_v2.adam import Adam
import tensorflow.python.keras.backend as B
from tensorflow.python.keras.models import Model
import tensorflow as tf
from tensorflow.python.keras.regularizers import l2
import tensorflow.python.keras as keras
from pathlib import Path

from dataset import MIND, Movielens
from utils import save_pickle, load_pickle, FILL_VALUE


# source: https://keunwoochoi.wordpress.com/2020/09/28/tensorflow2-keras-custom-loss-function-and-metric-classes-
# for-multi-task-learning/
def masked_mse_loss(y_true, y_pred):
    """
    MSE loss only calculated on observed, i.e. unmasked entries.
    :param y_true: True y with unobserved entries. ATTENTION: assumes that values that equal the global variable
    fill value are unobserved entries in input
    :param y_pred: Predicted y with no unobserved entries
    :return: Loss on observed entries
    """
    mask = B.cast(B.not_equal(y_true, FILL_VALUE), B.floatx())
    idcs = tf.where(mask)
    y_true_gathered = tf.gather_nd(y_true, idcs)
    y_pred_gathered = tf.gather_nd(y_pred, idcs)
    return B.mean(B.square(y_true_gathered - y_pred_gathered))


def masked_rmse_loss(y_true, y_pred):
    """
    RMSE loss only calculated on observed, i.e. unmasked entries.
    :param y_true: True y with unobserved entries. ATTENTION: assumes that values that equal the global variable
    fill value are unobserved entries in input
    :param y_pred: Predicted y with no unobserved entries
    :return: Loss on observed entries
    """
    return B.sqrt(masked_mse_loss(y_true, y_pred))


class Autoencoder:
    def __init__(self, num_users, latent_dim, kernel_reg, learning_rate, model_path=None, history_path=None):
        # load the model since paths were given
        if model_path and history_path:
            # need to pass the custom loss function so that loading the model works properly
            self.model = keras.models.load_model(model_path, custom_objects={"masked_mse_loss": masked_mse_loss,
                                                                             "masked_rmse_loss": masked_rmse_loss})
            self._history = load_pickle(history_path)
            self.loaded = True
        # build model
        else:
            input = layers.Input(shape=(num_users,))
            hidden = layers.Dense(latent_dim, kernel_regularizer=l2(kernel_reg), bias_regularizer=l2(0),
                                  activation='relu')(
                input)
            output = layers.Dense(num_users, kernel_regularizer=l2(kernel_reg), bias_regularizer=l2(0))(hidden)
            self.model = Model(inputs=input, outputs=output)
            self.model.compile(optimizer=Adam(learning_rate=learning_rate), loss=masked_mse_loss,
                               metrics=[masked_rmse_loss])
            self._history = None
            self.loaded = False

    def train(self, X_train, X_validate, batch_size, train_epochs):
        self._history = self.model.fit(
            x=X_train,
            y=X_train,
            batch_size=batch_size,
            epochs=train_epochs,
            verbose=1,
            validation_data=(X_validate, X_validate),
        )

    def history(self):
        if self._history and not self.loaded:
            return self._history.history
        elif self._history and self.loaded:
            return self._history

    def save(self, model_path, history_path):
        self.model.save(model_path)
        save_pickle(self.history(), history_path)


def create_model_movie(dataset, test_split, batch_size, train_epochs, latent_dim, kernel_reg, learning_rate):
    movielens = Movielens(dataset, test_split)
    model_path = f"checkpoints/movielens_{dataset}_{train_epochs}_{latent_dim}_{learning_rate}_{test_split}_{kernel_reg}"
    history_path = f"pickles/movielens_{dataset}_{train_epochs}_{latent_dim}_{learning_rate}_{test_split}_{kernel_reg}_history.pickle"
    # only train if not previously trained
    if not Path(model_path).exists():
        # build model
        autoenc = Autoencoder(movielens.num_users, latent_dim, kernel_reg, learning_rate)
        # train the model
        autoenc.train(movielens.X_train, movielens.X_validate, batch_size, train_epochs)
        # save the model
        autoenc.save(model_path, history_path)
    else:
        # load if already trained
        autoenc = Autoencoder(movielens.num_users, latent_dim, kernel_reg, learning_rate, model_path=model_path,
                              history_path=history_path)
    return movielens, autoenc, model_path, history_path


def create_model_mind(dataset, test_split, batch_size, train_epochs, latent_dim, kernel_reg, learning_rate):
    mind = MIND(dataset, test_split)
    model_path = f"checkpoints/mind_{dataset}_{train_epochs}_{latent_dim}_{learning_rate}_{test_split}_{kernel_reg}"
    history_path = f"pickles/mind_{dataset}_{train_epochs}_{latent_dim}_{learning_rate}_{test_split}_{kernel_reg}_history.pickle"
    # only train if not previously trained
    if not Path(model_path).exists():
        # build model
        autoenc = Autoencoder(mind.num_users, latent_dim, kernel_reg, learning_rate)
        # train the model
        autoenc.train(mind.X_train, mind.X_validate, batch_size, train_epochs)
        # save the model
        autoenc.save(model_path, history_path)
    else:
        # load if already trained
        autoenc = Autoencoder(mind.num_users, latent_dim, kernel_reg, learning_rate, model_path=model_path,
                              history_path=history_path)
    return mind, autoenc, model_path, history_path
