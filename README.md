# Repostory of Code for the project *Dynamic Recommender Systems*

## Setup

Execute `setup.py`, which will create the required folders. Download the data sets specified in the next paragraph and put them into the corresponding directories.

## Data Sets
Data sets taken from https://grouplens.org/datasets/movielens/, more specifically:
+ MovieLens 1M: https://grouplens.org/datasets/movielens/1m/
+ MovieLens 100k: https://grouplens.org/datasets/movielens/100k/

## File Descriptions
+ `Autoencoder.py`: Standard `Autoencoder.py` that takes an incomplete vector of all observed user ratings for one movie and reconstructs the complete vector
+ `Autoencoder_forgetting_learning_rate.py`: Like `Autoencoder.py`, but retrained on for multiple "days" on 100 movies with a smaller learning rate than during the original training.
+ `Autoencoder_forgetting_noretrain.py`: Like `Autoencoder_learning_rate.py`, but accuracy tested on all data sets (training, validation, and test) without retraining on the new movies.
+ `Autoencoder_replay.py`: Like `Autoencoder_learning_rate.py` but retraining with replayed training samples instead of smaller learning rate.
+ `Autoencoder_replaytradeoff.py`: Like `Autoencoder.py`, but here the tradeoff is analyzed between the fraction of old movies during retraining and accuracy.
+ `Autoencoder_learningratetradeoff.py`: Like `Autoencoder_learningratetradeoff.py`, but here tradeoff between accuracy and learning rate.
+ `Autoencoder_epochtradeoff.py`: Like `Autoencoder_learningratetradeoff.py`, but here tradeoff between number of retraining epochs and accuracy.
+ `content_based_filtering.py`: Small script doing a content based recommendation.
+ `masking_test.py`: Small script to understand the masking in Keras.
+ `matrix_fact.py`: Matrix factorization with self-implemented gradient descent.
+ `matrix_fact_v2.py`: Matrix factorization using tensorflow.
+ `matrix_fact_v3.py`: Matrix factorization using a dense layer instead of dot product.
+ `plot.py`: Script to plot.

## Authors
+ Alexander Glavackij
