"""
File implementing a content-based filtering approach using cosine distance to calculate the nearest neighbors to a
query movie.
"""

from scipy import spatial
from utils import read_movies


def get_min_hash(s):
    s_list = s.split("|")
    hashes = map(hash, s_list)
    return min(hashes)


def query(df, movieId):
    query_genres = df.loc[df['movieId'] == movieId].loc[:, '(no genres listed)':'Western']
    df['sim'] = df.apply(lambda x: spatial.distance.cosine(query_genres, x.loc['(no genres listed)':'Western']), axis=1)
    df2 = df.sort_values('sim', ascending=True).head(20)
    return df2


if __name__ == '__main__':
    df = read_movies()
    df['minHash'] = df['genres'].apply(lambda x: get_min_hash(x))
    print(query(df, 1).to_string())
