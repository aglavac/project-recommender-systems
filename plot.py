from utils import load_pickle, plot_losses

import matplotlib.pyplot as plt
# import seaborn as sns


def plot_epoch_tradeoff(dataset):
    fig, ax = plt.subplots(figsize=(10, 5))
    data = load_pickle(f"pickles/{dataset}_forgetting_epoch_tradeoff.pickle")
    ax.plot(data['epochs'], data['mean_train_rmse'])
    ax.plot(data['epochs'], data['mean_valid_rmse'])
    ax.plot(data['epochs'], data['mean_test_rmse'])
    ax.legend(['train', 'valid', 'test'], loc='upper left')
    ax.set_xlabel('# Retrain Epochs')
    ax.set_ylabel('Mean RMSE')
    ax.set_title('Retrain Epochs vs. Mean RMSE')
    plt.savefig('pdfs/epoch_rmse_tradeoff.pdf', bbox_inches='tight')
    plt.savefig('pdfs/epoch_rmse_tradeoff.png', bbox_inches='tight')
    plt.show()


def plot_replay_tradeoff(dataset):
    fig, ax = plt.subplots(figsize=(10, 5))
    data = load_pickle(f"pickles/{dataset}_forgetting_replay_tradeoff.pickle")
    ax.plot(data['replays'], data['mean_train_loss'])
    ax.plot(data['replays'], data['mean_valid_loss'])
    ax.plot(data['replays'], data['mean_test_loss'])
    ax.legend(['train', 'valid', 'test'], loc='upper left')
    ax.set_xlabel('Replay Fraction')
    ax.set_ylabel('Mean Loss')
    ax.set_title('Replay Fraction vs. Mean Loss')
    plt.savefig('pdfs/replay_rmse_tradeoff.pdf', bbox_inches='tight')
    plt.savefig('pdfs/replay_rmse_tradeoff.png', bbox_inches='tight')
    plt.show()


def plot_learningrate_tradeoff(dataset):
    fig, ax = plt.subplots(figsize=(10, 5))
    data = load_pickle(f"pickles/{dataset}_forgetting_learningrate_tradeoff.pickle")
    ax.plot(data['learning_rates'], data['mean_train_loss'])
    ax.plot(data['learning_rates'], data['mean_valid_loss'])
    ax.plot(data['learning_rates'], data['mean_test_loss'])
    ax.legend(['train', 'valid', 'test'], loc='upper left')
    ax.set_xlabel('Learning Rate')
    ax.set_ylabel('Mean Loss')
    ax.set_title('Learning Rate vs. Mean Loss')
    ax.set_xscale('log')
    plt.savefig('pdfs/learningrate_rmse_tradeoff.pdf', bbox_inches='tight')
    plt.savefig('pdfs/learningrate_rmse_tradeoff.png', bbox_inches='tight')
    plt.show()


def plot_losses_(dataset):
    data = load_pickle(f"pickles/{dataset}_forgetting_noretrain.pickle")
    fig, ax = plt.subplots(figsize=(10, 5))
    ax.plot(data["mean_train_loss"])
    ax.plot(data["mean_valid_loss"])
    ax.plot(data["mean_test_loss"])
    ax.set_xlabel("Day")
    ax.set_ylabel("Loss")
    ax.set_title("Loss for never seen Data Sets")
    ax.legend(['train', 'valid', 'test'], loc='upper left')
    fig.tight_layout()
    plt.savefig('pdfs/loss_no_retrain.pdf', bbox_inches='tight')
    plt.savefig('pdfs/loss_no_retrain.png', bbox_inches='tight')
    plt.show()


def plot_gradients():
    data = load_pickle("pickles/kernel_weights_gradients.pickle")

    fig, axs = plt.subplots(3, figsize=(15, 10))
    sns.heatmap(data['x'], vmin=data["minmin"], vmax=data["maxmax"], annot=True, ax=axs[0], square=True, fmt='.2f',
                annot_kws={'rotation': 90})
    axs[0].set_title('Input')
    axs[0].set_xlabel("User")
    axs[0].set_ylabel("Row for one Movie")
    sns.heatmap(data["grads"][0], vmin=data["minmin"], vmax=data["maxmax"], annot=True, ax=axs[1], fmt='.2f')
    axs[1].set_title('Kernel Gradients')
    axs[1].set_xlabel("Hidden Node")
    axs[1].set_ylabel("Weight")
    sns.heatmap(data["weights_to_draw"], vmin=data["minmin"], vmax=data["maxmax"], annot=True, ax=axs[2], fmt='.2f')
    axs[2].set_title('Kernel Weights')
    axs[2].set_xlabel("Hidden Node")
    axs[2].set_ylabel("Weight")
    fig.tight_layout()
    plt.show()


if __name__ == '__main__':
    plot_epoch_tradeoff("MINDsmall_train")
    # plot_losses_()
    # plot_replay_tradeoff()
    # plot_learningrate_tradeoff()
    # plot_gradients()
