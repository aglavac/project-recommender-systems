"""
Sets up the files structure for this project.
"""

from pathlib import Path

Path("checkpoints").mkdir(parents=True, exist_ok=True)
Path("data/movielens_1m").mkdir(parents=True, exist_ok=True)
Path("data/movielens_100k").mkdir(parents=True, exist_ok=True)
Path("data/MINDsmall").mkdir(parents=True, exist_ok=True)
Path("pickles").mkdir(parents=True, exist_ok=True)
Path("pdfs").mkdir(parents=True, exist_ok=True)
