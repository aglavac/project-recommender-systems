from utils import read_ratings, create_mappings
from sklearn.model_selection import train_test_split
from utils import MIND_SMALL, MIND_LARGE, FILL_VALUE
import pandas as pd
import os
import numpy as np


class Dataset:
    def __init__(self):
        # load the dataset
        self.df = None
        self.num_users = None
        self.num_movies = None

        self.df_train = None
        self.df_test = None
        self.df_validate = None

        self.X_train = None
        self.X_validate = None
        self.X_test = None

    def __str__(self):
        return "dataset"


class Movielens(Dataset):
    def __init__(self, dataset, test_split):
        self.dataset = dataset
        super(Movielens, self).__init__()
        # load the dataset
        self.df, _ = read_ratings(dataset)
        self.num_users = len(self.df['userId'].unique())
        self.num_movies = len(self.df['movieId'].unique())
        print(f"Number of movies: {self.num_movies}\nNumber of users: {self.num_users}")
        # do the id mappings, since Ids not continuous
        mapping_X_to_df_movies, mapping_X_to_df_users = create_mappings(self.df)
        self.mapping_df_to_X_movies = {value: key for key, value in mapping_X_to_df_movies.items()}
        self.mapping_df_to_X_users = {value: key for key, value in mapping_X_to_df_users.items()}
        self.df['userId'] = self.df['userId'].apply(lambda x: self.mapping_df_to_X_users[x])
        self.df['movieId'] = self.df['movieId'].apply(lambda x: self.mapping_df_to_X_movies[x])
        # build train and test set
        self.df_movielens_pivoted = self.df.pivot(index='movieId', columns='userId',
                                                  values='rating').sort_index()
        self.df_movielens_pivoted = self.df_movielens_pivoted.fillna(value=FILL_VALUE)

        self.df_train, self.df_test = train_test_split(self.df_movielens_pivoted, test_size=test_split, random_state=12)
        self.df_train, self.df_validate = train_test_split(self.df_train, test_size=0.1, random_state=12)

        self.df_train = self.df_train.sort_index()
        self.df_validate = self.df_validate.sort_index()
        self.df_test = self.df_test.sort_index()

        self.X_train = self.df_train.to_numpy()
        self.X_validate = self.df_validate.to_numpy()
        self.X_test = self.df_test.to_numpy()

    def __str__(self):
        return f"movielens_{self.dataset}"


class MIND(Dataset):
    def __init__(self, dataset, test_split):
        self.dataset = dataset
        super(MIND, self).__init__()
        # load the dataset
        self.df = MIND.load_mind(dataset)
        self.num_news = self.df["article_id_cont"].nunique()
        self.num_users = self.df["user_id_cont"].nunique()
        print(f"Number of news articles: {self.num_news}\nNumber of users: {self.num_users}")
        # pivot the dataframe
        self.df_mind_pivoted = MIND.pivot_mind(self.df)
        # build train and test set
        self.df_train, self.df_test = split_df_chrono(self.df_mind_pivoted, test_split)
        self.df_train, self.df_validate = split_df_chrono(self.df_train, 0.1)

        # self.df_train = self.df_train.sort_index()
        # self.df_validate = self.df_validate.sort_index()
        # self.df_test = self.df_test.sort_index()

        self.X_train = self.df_train.to_numpy()
        self.X_validate = self.df_validate.to_numpy()
        self.X_test = self.df_test.to_numpy()

    def __str__(self):
        return f"{self.dataset}"

    @staticmethod
    def load_mind(dataset):
        if "small" in dataset:
            df = pd.read_csv(os.path.join(MIND_SMALL, dataset, 'news_user_cleaned.csv'), sep=",", index_col=0)
        else:
            df = pd.read_csv(os.path.join(MIND_LARGE, dataset, 'news_user_cleaned.csv'), sep=",", index_col=0)
        return df

    @staticmethod
    def pivot_mind(df):
        n_news_mapping = df["article_id_cont"].nunique()
        n_user_mapping = df["user_id_cont"].nunique()
        news_user = np.zeros((n_news_mapping, n_user_mapping))
        for _, row in df.iterrows():
            news_user[row['article_id_cont'], row['user_id_cont']] = 1
        df_news_user = pd.DataFrame(news_user)
        return df_news_user


def split_df_chrono(df, test_split):
    """
    Splits the given data frame chronologically (rows with higher index are later chronologically)
    """
    if isinstance(test_split, int):
        df_test = df.tail(test_split)
        remaining_rows = len(df) - test_split
        df_train = df.head(remaining_rows)

    elif isinstance(test_split, float):
        test_tail = int(len(df) * test_split)
        df_test = df.tail(test_tail)
        remaining_rows = len(df) - test_tail
        df_train = df.head(remaining_rows)

    else:
        raise ValueError('Wrong test_split!')

    return df_train, df_test


def prepare_mind(dataset):
    behaviors_path = os.path.join('data', 'MINDsmall', dataset, 'behaviors.tsv')
    df_behaviors = pd.read_table(
        behaviors_path,
        header=None,
        names=['impression_id', 'user_id', 'time', 'history', 'impressions'])
    df_behaviors = df_behaviors[['user_id', 'time', 'history']]
    df_behaviors.loc[:, 'history'] = df_behaviors['history'].str.split(' ')
    df_behaviors = df_behaviors.explode('history', ignore_index=True)
    df_behaviors = df_behaviors.drop_duplicates(subset=['user_id', 'history'])
    df_behaviors['seen'] = 1
    mapping_user_id_id = {value: idx for idx, value in enumerate(df_behaviors['user_id'].unique())}
    mapping_news_id_id = {value: idx for idx, value in enumerate(df_behaviors['history'].unique())}
    df_behaviors['user_id_cont'] = df_behaviors.apply(lambda x: mapping_user_id_id[x['user_id']], axis=1)
    df_behaviors['news_id_cont'] = df_behaviors.apply(lambda x: mapping_news_id_id[x['history']], axis=1)
    df_behaviors.to_csv(os.path.join('data', 'MINDsmall', dataset, f"news_user_cleaned.csv"))


if __name__ == '__main__':
    prepare_mind("MINDsmall_train")
